<?php

#
#	This is the config file that is ran for every function. 
#
#	This config is included BEFORE any before_call, init, action, or 
#	_after_call methods
#	
#	This means that plugins can utilize the contents of this config
#

#
#	Set up Hash
#

Light_Hash::set('method', 'sha256');
Light_Hash::set('key', 'RXm3UrZdkxm3BGN264RstEyunSVWYNo');

#
#	Set up database
#

Light_Datasources::set
( 
	'default', 
	array( 
		'type' => 'mysql', 
		'name' => 'lightmvc', 
		'user' => 'lightmvc', 
		'pass' => 'lightmvc', 
		'addr' => 'localhost' 
	) 
);

#
#	Set up Environment Checks
#

Light_Environment::set('local', 'local.lightmvc.org');
Light_Environment::set('qa', 'qa.lightmvc.org');
Light_Environment::set('prod', 'www.lightmvc.org');

define('LIGHT_ENVIRONMENT', Light_Environment::current());

#
#	Define Routes
#

Light_Router::add('default', '/:controller/:action/:id/');

#
#	Register Plugins
#

Light_Plugin::register('blueprintcss');
#Light_Plugin::register('jquery');
#Light_Plugin::register('jquery_ui');
#Light_Plugin::register('jquery_lightbox');
#Light_Plugin::register('jquery_validation');
