<?php

#
#	Each Application can have it's own config file
#
#	This config is included BEFORE any before_call, init, action, or _after_call methods
#	but AFTER the global config file.
#	
#	This means that plugins can utilize the contents of this config as well as the global.
#