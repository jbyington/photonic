<!DOCTYPE HTML>
<html>
	<head>
		<title>
			<?php echo $this->get_title(); ?>
		</title>
		<?php echo $this->css() ?>
		<?php echo $this->js() ?>
	</head>
	<body>
		<?php echo $this->get_content(); ?>
	</body>
</html>
