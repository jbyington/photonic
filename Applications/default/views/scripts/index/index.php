<style type="text/css" media="all">
	body { background-image: url('/images/lightmvc/blue_detail.png'); background-repeat: repeat-x; }
	.main { height:100px; background-color: transparent; line-height:100px; vertical-align:middle; }
	.main img { margin-top:15px; }
	.sub { height:40px; background-color: transparent; }
	.footer { height:48px; background-color: #002d8a; background-image: url(/images/lightmvc/blue_detail.png); text-align:center; }
	.footer span { color:#ffffff; line-height:48px; vertical-align:middle; }	
</style>

<div class="container">
	<div class="span-24 main last">
		<img src="<?php echo SITE_ADDRESS ?>/images/shared/lightmvc-logo.png" alt="<?php echo SITE_NAME ?>" />
	</div>
	<div class="span-24 sub last">
		<br />
	</div>

	<div class="span-24 prepend-top append-bottom last">
		<?php echo $this->partial('example'); ?>
	</div>
	
	<div class="span-24 prepend-top append-bottom last">
		<h3>Current Route</h3>
		<?php debug(Light_Router::$route_array) ?>
	</div>

	<div class="span-24 footer last">
		<span>&copy; 2010. All Rights Reserved.</span>
	</div>
</div>