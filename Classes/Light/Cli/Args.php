<?php

class Light_Cli_Args
{	
	public static $args;

	public static function clean( $arg_array )
	{
		foreach( $arg_array as $value )
		{
			if( strpos($value, '=') != false )
			{
				$value = str_replace('--', '', $value);
				$value = explode('=', $value);
				self::$args [$value[0]]= $value[1];
			}
			else
			{
				self::$args []= $value;
			}
		}
		array_shift(self::$args);
		return self::$args;
	}	
}