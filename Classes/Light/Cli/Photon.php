<?php

class Light_Cli_Photon
{
	protected $tests = array();
	protected $filters = array();
	
	public function __construct( $argv )
	{
		$args = Light_Cli_Args::clean( $argv );

		$this->set_tests( TEST_PATH );

		if( !empty($args) )
		{
			$this->set_filters( $args );
		}
		
		Light_Cli_Io::out(`clear`);
		
		$this->do_unit_testing();

	}

	protected function do_unit_testing()
	{	
		$filters = $this->get_filters();
		$tests = $this->get_tests();

		foreach( $tests as $test )
		{
			if( strpos($test['name'], '.php') !== false )
			{
				$test_file = str_replace( '/', '_', str_replace( TEST_PATH . '/', '', $test['full_path'] ) );

				if( empty($filters ) || in_array( $test_file, $filters ) )
				{
					include $test['full_path'];
					$class = basename($test_file, '.php') . '_test';

					$test = new $class;
					$test->_start();
				}
			}
		}
	}

	protected function get_filters()
	{
		return $this->filters;
	}

	protected function set_filters( $arg_array )
	{
		$this->filters = $arg_array;
	}

	protected function get_tests()
	{
		return $this->tests;
	}

	protected function set_tests( $path )
	{
		$this->tests = Light_Directory::rscan( $path );
	}	
}