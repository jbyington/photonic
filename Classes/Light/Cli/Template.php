<?php

class Light_Cli_Template
{	
	public function controller( $name )
	{
		return 
"<?php

class {$name}_controller extends Light_Controller_Action
{
	protected function init() {}
					
	public function index()
	{

	}
					
}";
	}

	public function helper( $name )
	{
		return 
"<?php

class {$name}
{
	public function __construct(){}

	public function {$name}( )
	{
		return; 
	}
}";
		}

		public function model( $name )
		{
			return 
"<?php

class {$name}
{
	public function __construct()
	{

	}
}";
	}
	
	public function model_crud( $name )
	{
		return 
"<?php

class {$name}
{
	public function __construct()
	{

	}
	
	public function insert()
	{
		
	}
	
	public function update()
	{
		
	}
	
	public function delete()
	{
		
	}
	
}";
	}
	
	public function layout()
	{
		return 
'<!DOCTYPE HTML>
<html>
	<head>
		<title>
			<?php echo $this->get_title(); ?>
		</title>
		<?php echo $this->css() ?>
		<?php echo $this->js() ?>
	</head>
	<body>
		<?php echo $this->get_content(); ?>
	</body>
</html>';
	}

}