<?php

class Light_Data
{		
	public static function debug( $var )
	{
		echo '<pre>', print_r($var, true), '</pre>';
	}

	public static function sanitize( $input )
	{
		$output = '';
		if( is_array($input) )
		{
			foreach( $input as $var => $val ) 
			{
				$output[$var] = self::sanitize($val);
			}
		}
		else 
		{
			if( get_magic_quotes_gpc() )
			{
				$input = stripslashes($input);
			}
			$output  = self::cleanInput($input);
		}
		return $output;
	}
	
	protected static function cleanInput($input) 
	{
		$output = '';		
		$search = array
		(
			'/<script[^>]*?>.*?<\/script>/si',   // Strip out javascript
			'/<[\/\!]*?[^<>]*?>/si',            // Strip out HTML tags
			'/<style[^>]*?>.*?<\/style>/siU',    // Strip style tags properly
			'/<![\s\S]*?--[ \t\n\r]*>/'         // Strip multi-line comments
		);
	 
		$output = preg_replace($search, '', $input);
		return $output;
	}
	
}