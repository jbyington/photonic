<?php

class Light_Database
{
	protected $type = false;
	protected $driver = false;
	protected $db = false;

	public function __construct( $connection_information = array() )
	{		
		$this->type = $connection_information['type'];
		switch($this->type) 
		{
			case 'mysql':
				$this->driver = 'Light_Database_MySQL';
				break;

			case 'mysqli':
				$this->driver = 'Light_Database_MySQLi';
				break;
				
			case 'mssql':
				$this->driver = 'Light_Database_MsSQL';
				break;

			case 'pgsql':
				$this->driver = 'Light_Database_PgSQL';
				break;
				
			default:
				break;
		}
		
		if( $this->driver === false )
		{
			return false;
		}
		
		$this->db = new $this->driver( $connection_information );
		
	}
	
	public function __call( $function, $arguments )
	{
		$driver_method = array( $this->db , $function );

		return call_user_func_array( $driver_method, $arguments );			
	}
			
}
