<?php
	
class Light_Database_MySQL
{
	public $last_insert_id = 0;

	private $connection_information;
	private $link = false;
	
	public function __construct( $conn = array() )
	{
		$this->connection_information = $conn;
		return $this->link;
	}
		
	public function connect()
	{
		if( $this->link === false )
		{
			if( !empty($this->connection_information) )
			{
				$this->link = mysql_connect( $this->connection_information['addr'], $this->connection_information['user'], $this->connection_information['pass']) or die('Could not connect: ' . mysql_error() );
				mysql_select_db( $this->connection_information['name'], $this->link );
				
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	public function disconnect()
	{
		if( $this->link !== false )
		{
			mysql_close( $this->link );
			$this->link = false;
		}
		else
		{
			return false;
		}
	}

	public function escape( $string )
	{
		$this->connect();
		return mysql_real_escape_string( $string );
	}
	
	public function quote( $string )
	{
		$this->connect();
		return "'" . mysql_real_escape_string( $string ) . "'";
	}

	public function select( $sql )
	{
		return $this->_select( $sql );
	}

	public function selectRow( $sql )
	{
		return reset( $this->_select($sql) );
	}
	
	public function selectOne( $sql )
	{
		return reset( reset( $this->_select($sql) ) );
	}
	
	public function update( $sql )
	{
		return $this->_query( $sql );
	}	

	public function insert( $sql )
	{
		return $this->_query( $sql );
	}
	
	public function delete( $sql )
	{
		return $this->_query( $sql );
	}
	
	public function currentval( $sequence )
	{
		return $this->selectOne( "select value from sequences where name = '$sequence'" );
	}

	public function nextval( $sequence )
	{
		$temp = $this->update( "update sequences set value = value + step where name = '$sequence'" );
		return $this->currentval( $sequence );
	}
	
	//---------------------------------------------------------------------------------
	
	private function _select( $sql )
	{
		$this->connect();
	
		$temp = array();

		if( $results = mysql_query( $sql, $this->link ) )
		{
			while( $row = mysql_fetch_assoc($results) ) 
			{
				$temp[] = $row;
			}
		}
		
		$this->disconnect();

		return $temp;	
	}
	
	private function _query( $sql )
	{
		$this->connect();
		
		$success = false;

		if( $results = mysql_query($sql, $this->link) )
		{ 
			$this->last_insert_id = mysql_insert_id($this->link);
			$success = true; 
		}

		$this->disconnect();
		
		return $success;	
	}	
	
}