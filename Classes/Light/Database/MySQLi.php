<?php
	
class Light_Database_MySQLi
{
	public $last_insert_id = 0;
		
	private $connection_information;
	private $link = false;

	public function __construct( $conn = array() )
	{
		$this->connection_information = $conn;
		return $this->link;
	}

	public function connect()
	{			
		if( $this->link === false )
		{
			if( !empty($this->connection_information) )
			{
				$this->link = new mysqli( $this->connection_information['addr'], $this->connection_information['user'], $this->connection_information['pass'], $this->connection_information['name'], $this->connection_information['port'], $this->connection_information['socket'] ); 
				if( $this->link->connect_error ) 
				{ 
					die( 'Could not connect to mysqli. Reason: ' . mysqli_connect_error() ); 
				}					
				return true;
			}
			else
			{
				return false;
			}
		}			
	}

	public function disconnect()
	{			
		if( $this->link !== false )
		{
			$this->link->close();
			$this->link = false;
		}
		else
		{
			return false;
		}
	}

	public function escape( $string )
	{
		$this->connect();
		return mysqli_real_escape_string($string);
	}
	
	public function quote( $string )
	{
		$this->connect();
		return "'" . mysqli_real_escape_string($this->link,$string) . "'";
	}
	
	public function select( $sql )
	{
		return $this->_select( $sql );
	}

	public function selectRow( $sql )
	{
		return reset( $this->_select($sql) );
	}
	
	public function selectOne( $sql )
	{
		return reset( reset( $this->_select($sql) ) );
	}
	
	public function update( $sql )
	{
		return $this->_query( $sql );
	}	

	public function insert( $sql )
	{
		if( $this->_query( $sql ) )
		{
			return $this->last_insert_id;
		}
		else
		{
			return false;
		}
	}
	
	public function delete( $sql )
	{
		return $this->_query( $sql );
	}
	
	public function currentval( $sequence )
	{
		return $this->selectOne( "select value from sequences where name = '$sequence'" );
	}

	public function nextval( $sequence )
	{
		$temp = $this->update( "update sequences set value = value + step where name = '$sequence'" );	
		$value = $this->currentval( $sequence );

		return $value;
	}
	
	//---------------------------------------------------------------------------------
	
	private function _select( $sql )
	{
		$this->connect();
	
		$temp = array();

		if( $results = $this->link->query($sql) )
		{
			while( $row = $results->fetch_assoc() ) 
			{
				$temp[] = $row;
			}
		}
		
		$this->disconnect();
		
		return $temp;	
	}
	
	private function _query( $sql )
	{
		$this->connect();
	
		$success = false;
		
		if( $results = $this->link->query($sql) )
		{ 
			$this->last_insert_id = $this->link->insert_id;

			$success = true; 
		}
		
		$this->disconnect();
		
		return $success;	
	}	
	
}