<?php

class Light_Ftp
{
	protected 	$ftp_handle,
				$nb_handle,
	 			$port = '21', 
				$timeout = '90',
				$ssl = false,
				$async = false,
				$pasv = false;
	
	
	public function __construct(){}
	
	public function __destruct()
	{ 
		ftp_close( $this->ftp_handle );
	}
	
	public function __set( $property, $value )
	{
		$this->$property = $value;
	}
	
	public function __get( $property )
	{
		return $this->$property;
	}
	
	public function connect( $host, $username = false, $password = false )
	{
		if( $this->ssl )
		{
			$this->ftp_handle = ftp_ssl_connect( $host, $this->port, $this->timeout );
		}
		else
		{
			$this->ftp_handle = ftp_connect( $host, $this->port, $this->timeout );
		}

		if( is_resource($this->ftp_handle) ) 
		{
			$login_status = true;
			if( $username && $password )
			{
				$login_status = ftp_login( $this->ftp_handle, $username, $password );
			}
			ftp_pasv( $this->ftp_handle, $this->pasv );

			return $login_status;
		}
		
		return false;
	}
	
	public function get( $local, $remote, $binary = false )
	{
		$mode = ( $binary ) ? FTP_BINARY : FTP_ASCII;
		if( $this->async )
		{
			if( is_resource($local) )
			{
				$this->nb_handle = ftp_nb_fget( $this->ftp_handle, $local, $remote, $mode );
			}
			else
			{
				$this->nb_handle = ftp_nb_get( $this->ftp_handle, $local, $remote, $mode );
			}
			return $this->nb_handle;
		}
		else
		{
			if( is_resource($local) )
			{
				$result = ftp_get( $this->ftp_handle, $local, $remote, $mode );
			}
			else
			{
				$result = ftp_get( $this->ftp_handle, $local, $remote, $mode );
			}
			return $result;
		}
	}
	
	public function put( $remote, $local, $binary = false )
	{
		$mode = ( $binary ) ? FTP_BINARY : FTP_ASCII;
		if( $this->async )
		{
			if( is_resource($local) )
			{
				$this->nb_handle = ftp_nb_fput( $this->ftp_handle, $remote, $local, $mode );
			}
			else
			{
				$this->nb_handle = ftp_nb_put( $this->ftp_handle, $remote, $local, $mode );
			}
			return $this->nb_handle;
		}
		else
		{
			if( is_resource($local) )
			{
				$result = ftp_put( $this->ftp_handle, $remote, $local, $mode );
			}
			else
			{
				$result = ftp_put( $this->ftp_handle, $remote, $local, $mode );
			}
			return $result;
		}
	}
	
	public function delete( $path )
	{
		return ftp_delete( $this->ftp_handle, $path );
	}

	public function status()
	{
		$this->nb_handle = ftp_nb_continue( $this->ftp_handle );
		return $this->nb_handle;
	}

	public function chmod( $filename, $mode )
	{
		return ftp_chmod( $this->ftp_handle, $mode, $filename );
	}

	public function mkdir( $directory )
	{
		return ftp_mkdir( $this->ftp_handle, $directory );
	}
	
	public function rmdir( $directory )
	{
		return ftp_rmdir( $this->ftp_handle, $directory );
	}
	
	public function chdir( $directory )
	{
		return ftp_chdir( $this->ftp_handle, $directory );
	}

	public function nlist( $directory )
	{
		return ftp_nlist( $this->ftp_handle, $directory );
	}

	public function rawlist( $directory, $recursive = false )
	{
		return ftp_rawlist( $this->ftp_handle, $directory, $recursive );
	}
	
	public function rename( $oldname, $newname )
	{
		return ftp_rename( $this->ftp_handle, $oldname, $newname );
	}
	
	public function cdup()
	{
		return ftp_cdup( $this->ftp_handle );
	}

	public function pwd()
	{
		return ftp_pwd( $this->ftp_handle );
	}

	public function size( $filename )
	{
		return ftp_size( $this->ftp_handle, $filename );
	}

}