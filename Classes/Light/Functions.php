<?php

class Light_Functions
{
	public function __construct()
	{
		return true;
	}
	
	public static function init()
	{			
		function e( $input )
		{
			echo $input;
		}
		
		function pre( $input )
		{
			return Light_String::pre( $input );
		}
		
		function pr( $input )
		{
			print_r($input);
		}
		
		function prs( $input )
		{
			return print_r($input, true);
		}
		
		function debug( $input )
		{
			Light_Data::debug( $input );
		}
	}
}