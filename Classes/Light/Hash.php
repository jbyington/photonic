<?php

class Light_Hash
{
	private static $method = 'sha256';
	private static $key = '';
	
	public function __construct(){}
	
	public static function set( $key, $value )
	{
		self::$$key = $value;
	}
	
	public static function get( $key )
	{
		if( !isset(self::$$key) )
		{
			return false;
		}
		return self::$$key;
	}
	
	public static function hash( $string )
	{
		return hash_hmac( self::get('method'), $string, self::salt($string, self::get('key')) );
	}
	
	public static function salt( $string, $key = '' ) 
	{
		$hash = '';
		
	    if( !empty($string) )
		{
			foreach( str_split($string) as $char )
			{
	        	$hash = hash( self::get('method'), $hash.$key.$char );
	    	}
		}

	    return $hash;
	}		
	
}