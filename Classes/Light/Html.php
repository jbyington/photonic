<?php

class Light_Html
{		
	public static function ol( $array, $classname = '' )
	{	
		return "<ol class=\"$classname\">" . self::li( $array ) . '</ol>';
	}		

	public static function ul( $array, $classname = '' )
	{		
		return "<ul class=\"$classname\">" . self::li( $array ) . '</ul>';
	}

	public static function li( $array, $classname = '' )
	{
		$html = '';
		foreach( $array as $k => $v )
		{
			$html .= "<li class=\"$classname\">$v</li>";
		}
		return $html;
	}

	public static function table ( $array, $dimensions = 2,  $classname = '' )
	{	
		$html_table = '';
		
		if( !empty($array) )
		{ 

			$table_headers = array_keys( reset($array) );
			
			$html_table .= '<table class="'.$classname.'"><thead><tr>';
			
			foreach($table_headers as $key)
			{
				$html_table .= '<th>'.$key.'</th>';	
			}
			
			$html_table .= '</tr></thead><tbody>';

			$row_class = 'odd';
			foreach($array as $rownum => $row)
			{			
				$html_table .= '<tr class="'.$row_class.'">';
				
				foreach( $table_headers as $index => $key )
				{
					$html_table .= '<td>'.$row[$key].'</td>';	
				}
				
				$html_table .= '</tr>';
				
				if( $row_class == 'odd' ) 
				{				
					$row_class = 'even';
				}
				else
				{
					$row_class = 'odd';
				}
			}
			
			$html_table .= '</tbody></table>';
			
			return $html_table;
		}
	}
	
}