<?php

class Light_Lipsum
{
	protected static function get_ipsum_text( $stripped = false )
	{
		if( $stripped )
		{
			return    'lorem ipsum dolor sit amet consectetur adipisicing '
					. 'elit sed do eiusmod tempor incididunt ut labore et '
					. 'dolore magna aliqua ut enim ad minim veniam quis nostrud '
					. 'exercitation ullamco laboris nisi ut aliquip ex ea commodo '
					. 'consequat duis aute irure dolor in reprehenderit in voluptate '
					. 'velit esse cillum dolore eu fugiat nulla pariatur excepteur sint '
					. 'occaecat cupidatat non proident sunt in culpa qui officia '
					. 'deserunt mollit anim id est laborum';
		}
		else
		{
			return    'Lorem ipsum dolor sit amet, consectetur adipisicing '
					. 'elit, sed do eiusmod tempor incididunt ut labore et '
					. 'dolore magna aliqua. Ut enim ad minim veniam, quis nostrud '
					. 'exercitation ullamco laboris nisi ut aliquip ex ea commodo '
					. 'consequat. Duis aute irure dolor in reprehenderit in voluptate '
					. 'velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint '
					. 'occaecat cupidatat non proident, sunt in culpa qui officia '
					. 'deserunt mollit anim id est laborum.';
		}
	}

    public static function title( $words = 5, $offset = 0 )
    {
        $lipsum_array = explode( ' ', self::get_ipsum_text(true) );
        return ucwords( implode( ' ', array_slice($lipsum_array, $offset, $words) ) );
    }

    public static function text( $chars = 350 )
    {
        $lipsum_array = explode( ' ', self::get_ipsum_text() );
        $return = '';
        foreach( $lipsum_array as $word_part )
        {
            if( strlen($return) + strlen($word_part) > $chars )
            {
                $return .= $word_part;
                $return = substr( $return, 0, $chars - 3) . '...';
            }
            else if( strlen($return) + strlen($word_part) == $chars - 1 )
            {
                $return .= "$word_part.";
                break;
            }
            else if( strlen($return) + strlen($word_part) + 1 < $chars )
            {
                $return .= "$word_part ";
            }
        }
        
        return $return;
    }

}