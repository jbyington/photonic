<?php

# try and grab these files from pear
require_once 'Mail.php';
require_once 'Mail/mime.php';

class Light_Mail
{
	protected $mail = null;
	protected $mime = null;

	public $to = array();
	public $headers = array();


	public function __construct( )
	{
		$this->mime = new Mail_mime;
		$this->mime->setParam('eol',"\n"); #required to send through pear Mail
	}

	public function from( $address )
	{
		$this->mime->setFrom( $address );
		return $this;
	}

	public function reply_to( $address )
	{
		$this->headers[] = array( 'Reply-To' => $address );
		return $this;
	}

	public function to( $address )
	{
		$this->to[] = $address;
		$this->mime->addTo( $address );
		return $this;
	}

	protected function get_to()
	{
		return $this->mime->encodeRecipients( implode( ', ', $this->to ) );
	}

	public function cc( $address )
	{
		$this->mime->addCc( $address );
		return $this;
	}

	public function bcc( $address )
	{
		$this->mime->addBcc( $address );
		return $this;
	}

	public function subject( $text )
	{
		$this->mime->setSubject( $text );
		return $this;
	}

	public function message( $content, $type = 'html' )
	{
		switch( $type )
		{
			case 'html':
				$this->mime->setHTMLBody( $content );
				break;
			case 'text':
			default:
				$this->mime->setTXTBody( $content );
				break;
		}

		return $this;
	}


	public function attach( $file, $c_type = 'application/octet-stream', $name = '', $isfile = true,
							$encoding = 'base64', $disposition = 'attachment', $charset = '',
							$language = '', $location = '', $n_encoding = null, $f_encoding = null,
							$description = '', $h_charset = null )
	{
		if( !empty($file) )
		{
			$this->mime->addAttachment( $file, $c_type, $name, $isfile, $encoding, $disposition,
										$charset, $language, $location, $n_encoding, $f_encoding,
										$description, $h_charset );
		}
		return $this;
	}

	public function send()
	{
 		$this->mail = Mail::factory('Mail');

		$body = $this->mime->get();
		$headers = $this->mime->headers( $this->headers );

		return $this->mail->send( $this->get_to(), $headers, $body );
	}

}