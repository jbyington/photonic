<?php

class Light_Message
{
	public function __construct(){}

	public static function add( $mixed )
	{
		$messages = Light_Session::get('__messages__');

		if( is_null($messages) )
		{
			$messages = array();
		}

		$messages []= $mixed;

		Light_Session::set('__messages__', $messages);
	}

	public static function clear()
	{
		Light_Session::set('__messages__', array());
	}

	public static function get()
	{
		$messages = Light_Session::get('__messages__');
		if( empty($messages) )
		{
			$messages = array();
		}
		return $messages;
	}

}