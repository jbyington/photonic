<?php

class Light_Navigation
{
	public function __construct()
	{
		#hi mom!
	}
	
	/*
		Example:
					Focusing On
	                      \/
		Back  1  2  3  4  5  6  7  8  9 10 Next
		Back 11 12 13 14 15 16 17 18 19 20 Next
	*/
	public static function page_bar( $total_count = 0, $per_page = 10, $get = 'start', $slider = 10 )
	{
		$return = array();
		$current_offset = (($_GET[$get]) ? $_GET[$get] : 0);
		$current_page_number = $current_offset / $per_page;
		
		$params = array();
		parse_str( $_SERVER['QUERY_STRING'], $params );
		
		if( $current_offset - $per_page >= 0 )
		{
			$params[$get] = $current_offset - $per_page;
			$return['back'] = array( 'page' => 'Back', 'url' => '?'.http_build_query($params) );
		}
		else
		{
			$return['back'] = array( 'page' => 'Back', 'url' => false );				
		}
	
		$p = 1;	
		for( $i = 0; $i < $total_count; $i += $per_page )
		{
			$params[$get] = $i;
			$return['pages'] []=  array( 'page' => $p, "$get" => $i , 'url' => '?'.http_build_query($params) );
			$p++;
		}

		$num_pages = count( $return['pages'] );
		if( $current_page_number + 1 <= $slider / 2 )
		{
			$display_pages = range( 0 , $slider );
		}
		else if( $current_page_number + 1 >= $num_pages - $slider / 2 )
		{
			$display_pages = range( $num_pages - $slider - 1 , $num_pages - 1 );
		}
		else
		{
			$display_pages = range( $current_page_number - $slider / 2 , $current_page_number + $slider / 2 );
		}

		foreach( $return['pages'] as $index => $page )
		{
			if( !in_array($index, $display_pages) )
			{
				unset($return['pages'][$index]);					
			}
		}
		
		if( $current_offset + $per_page < $total_count )
		{
			$params[$get] = $current_offset + $per_page;
			$return['next'] = array( 'page' => 'Next', 'url' => '?'.http_build_query($params) );
		}
		else
		{
			$return['next'] = array( 'page' => 'Next', 'url' => false );				
		}
		return $return;
	}
}