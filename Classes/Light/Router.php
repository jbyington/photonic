<?php

class Light_Router 
{		
	protected static $router = false;
	public static $route_array = array();

	public static $module;
	public static $controller;
	public static $action;
	public static $id;
	public static $name;
	public static $params = array();

	public function __construct(){}

	protected static function get_router()
	{
		if( empty(self::$router) )
		{
			self::$router = new Light_Router_Regex();
		}
	}

	public static function init( $request_uri )
	{
		self::get_router();
		
		self::$route_array = self::$router->route( $request_uri );

		$non_params = array('module','controller','action','id','name');
		foreach( self::$route_array as $key => $value )
		{
			if( in_array($key, $non_params) )
			{
				self::$$key = $value;
			}
			else
			{
				self::$params[$key] = $value;				
			}

		}

		return self::$route_array;
	}
	
	public static function add( $name, $pattern, $params = array() )
	{
		self::get_router();
		self::$router->add( $name, $pattern, $params );
	}
}