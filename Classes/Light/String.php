<?php

class Light_String
{
	public static function pre( $string )
	{		
		return "<pre>$string</pre>";
	}

	public static function blockcode( $string )
	{		
		return "<code><pre>$string</pre></code>";
	}
	
	public static function dasherize( $string )
	{		
		return str_replace( ' ', '-', $string );
	}

	public static function undasherize( $string )
	{
		return str_replace( '-', ' ', $string );
	}
	
	public static function truncate( $string, $num_chars )
	{
		if( strlen($string) <= $num_chars )
		{
			return $string;
		}
		else
		{
			return substr( $string, 0, $num_chars - 1 ) . '&hellip;';
		}
	}		

	public static function extract_emails($list) {
		
		$pattern="/(\w+\.)*\w+@(\w+\.)*\w+(\w+\-\w+)*\.\w+/";
		
		preg_match_all($pattern,$list,$emails);

		if (is_array($emails[0])) {
			return $emails[0];
		}

		return array();

	}
}