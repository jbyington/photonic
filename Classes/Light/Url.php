<?php
	
class Light_Url
{					
	public static function redirect( $location = false )
	{
		if(!$location)
		{
			return false;
		}
		
		header("Location: $location");
		exit;
	}		
}