<?php

define('DEFAULT_LAYOUT_NAME', 'default');

class Light_View extends Light_View_Extension
{
	protected $_content 	= '';
	protected $_css 		= array();
	protected $_js 			= array();
	protected $_layout;
	protected $_title 		= '';
	protected $_meta 		= array();
	
	final public function __construct(  )
	{
		$this->set_layout( DEFAULT_LAYOUT_NAME );
		$this->set_script( Light_Router::$action );
	}

	final public function add_css( $path, $media = 'all', $condition = false )
	{
		$temp['src'] = $path;
		$temp['media'] = $media;
		$temp['condition'] = $condition;
		$this->_css[] = $temp;
	}
	
	final public function add_js( $path )
	{
		$this->_js[] = $path;
	}

	final public function add_meta ($name, $value) {
		$this->_meta[] = sprintf('<meta name="%s" content="%s" >',$name, $value);
	}

	final protected function meta(){
		return implode(PHP_EOL, $this->_meta);
	}

	final protected function js()
	{
		$js_string = '';
	
		foreach( $this->_js as $javascript )
		{
			$js_string .= '<script type="text/javascript" src="'.$javascript.'"></script>';
		}
		return $js_string;
	}
	
	final protected function css()
	{
		$css_string = '';
	
		foreach( $this->_css as $stylesheet )
		{
			if( $stylesheet['condition'] )
			{
				$css_string .= '<!--[if '.$stylesheet['condition'].']>';
			}
			$css_string .= '<link rel="stylesheet" type="text/css" href="'. $stylesheet['src'] . '" media="'.$stylesheet['media'].'" />';
			if( $stylesheet['condition'] )
			{
				$css_string .= '<![endif]-->';
			}			
		}
		return $css_string;
	}
		
	final public function _display( $error = false )
	{
		ob_start(); 	
			$this->_set_content( $error );
			include $this->_get_layout();
		$display = ob_get_contents(); 
		ob_end_clean();
		return $display;
	}

	final public function set_layout( $layout )
	{
		$path = APP_PATH . '/' . Light_Router::$module . '/views/layouts/' . $layout . '.php';

		if(file_exists($path))
		{
			$this->_set_layout( $path );
		}
	}

	final public function _set_layout( $layout )
	{
		$this->_layout = $layout;
	}

	final public function _get_layout()
	{
		return $this->_layout;
	}
	
	final public function set_title( $title )
	{
		$this->_title = $title;
	}		

	final public function get_title()
	{
		return $this->_title;
	}
		
	# -------------------------------------------------------------------------------------

	final public function set_script( $script )
	{
		$this->script = $script;
		$this->set_script_path( Light_Router::$module, Light_Router::$controller, $script );
	}	

	final public function get_script()
	{
		return $this->script;
	}		

	final public function set_script_path( $module = 'default', $controller, $action )
	{
		$this->script_path = APP_PATH . "/{$module}/views/scripts/{$controller}/{$action}.php";
	}

	final protected function _get_script_path()
	{
		return $this->script_path;
	}
	
	final protected function _set_content( $error = false )
	{
		ob_start(); 	
		if( $error )
		{
			include ERROR_PATH . "/{$error}.php";
		}
		else
		{
			include $this->_get_script_path();
		}
		$this->_content = ob_get_contents(); 
		ob_end_clean(); 			
	}

	final protected function get_content()
	{
		return $this->_content;
	}	
}
