<?php

class Light_View_Extension
{
	final public function __call( $function, $arguments )
	{
		return call_user_func_array( array( new Light_View_Helper(), $function ), $arguments );	
	}

	final protected function partial( $filename = '', $data = '' )
	{
		$module = Light_Router::$module;
		
		if( is_array($filename) )
		{
			$module = reset($filename);
			$filename = end($filename);
		}
		
		$path = APP_PATH . "/$module/views/partials/$filename.php";
		
		if( file_exists($path) )
		{
			$partial = new Light_View_Partial( $path );
			return $partial->init( $data );
		}
		
		return false;
	}

	final protected function partial_loop( $filename = '', $data = '' )
	{
		$content = '';
		foreach( $data as $i => $datum )
		{
			if( is_array($datum) )
			{
				$datum['_index_'] = $i;	
			}
			else
			{
				$datum->_index_ = $i;						
			}
			$content .= $this->partial( $filename, $datum );
		}
		return $content;
	}
}