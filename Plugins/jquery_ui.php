<?php
	class jquery_ui extends Light_Controller_Plugin
	{	
		public function init()
		{
			$this->view()->add_js('/libraries/jquery-ui/js/jquery-ui-1.8.13.custom.min.js');
			$this->view()->add_css('/libraries/jquery-ui/css/custom-theme/jquery-ui-1.8.13.custom.css','screen');
		}	
	}