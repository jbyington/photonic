LightMVC
========
LightMVC is a lightweight MVC framework similar in structure to the Zend Framework, but relying on convention rather than configuration. LightMVC borrows the good concepts from Zend, Code Igniter, Cake, and other frameworks, and tries to leave the bad behind.

License
-------

Copyright (c) 2010 LightMVC

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


The Story so far
----------------

LightMVC uses Convention over Configuration to make your MVC project go as smoothly as possible. There are no giant config files for core functionality, there are not one hundred places files and libraries can go. The entire framework is written to be small, light, and fast.

Our goal is for LightMVC to become a viable first choice in frameworks when setting up a website. We are committed to that goal.


Roadmap
-------

* BETA   - All major components are working, but we still need to iron out some kinks with pathing and environments.
* v1.0.0 - Reached at the beginning of the summer 2010.
* v1.0.1 - Small bug fix for the 1.0 branch
* v1.1.0 - Major file restructuring, code changes to "lean it up". Official Plugin support.
* v1.2.0 - Config restructuring, HTTP config samples included, Logs directory created, LTBL more advanced, Photon Unit Testing framework created, various fixes and enhancements.
* v1.3.0 - Core framework restructing. Light_Array now available as an object. Shortcut functions introduced. e(), pr(), prs(), debug(), and Light_Extend()
* v1.4.0 - Plugin support, suggested libraries folder created in /public
* v1.4.1 - bug fix to v1.4
* v2.0.0 - Now out of beta! Custom routes, new Light_Mail, new folder structure, better plugins
