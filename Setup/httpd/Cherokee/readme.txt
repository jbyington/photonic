===============================================================================

Cherokee Web Server Instructions
compiled 10/12/2010

===============================================================================

NOTE: More information may be required for this file.

===============================================================================

Cherokee is shipped with a configuration Wizard that can be used both to deploy
the application under a dedicated Virtual Server and under a web folder from 
one already existent.

Launch cherokee-admin, and proceed to the Virtual servers section.

Now you can either enter the chosen one to install under a web folder, or 
directly click on the Add button of the Virtual Servers panel to create a 
customised virtual server. In any case you will find the wizard under the 
Platforms category.

After that you will be asked for the minimal information required to correctly
configure your server in a totally transparent way.

And that’s it. You are done with the configuration! You can now access your application.