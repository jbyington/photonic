<?php
	
	/**
	 * @var string contains the full file path to lightmvc
	 */
	define('SERVER_ROOT', dirname(__FILE__));

	/**
	 * @var string contains the full file path to Public
	 */
	define('PUBLIC_PATH', SERVER_ROOT.'/Public');

	/**
	 * @var string contains the full file path to Modules folder
	 */
	define('MODULE_PATH', SERVER_ROOT.'/Modules');
	
	/**
	 * @var string contains the full file path to Classes folder
	 */
	define('CLASS_PATH', SERVER_ROOT.'/Classes');

	/**
	 * @var string contains the full file path to Configs folder
	 */
	define('CONFIG_PATH', SERVER_ROOT.'/Configs');	
	
	/**
	 * @var string contains the full file path to Errors folder
	 */
	define('ERROR_PATH', SERVER_ROOT.'/Errors');	
	
	/**
	 * @var string contains the full file path to Plugins folder
	 */
	define('PLUGIN_PATH', SERVER_ROOT.'/Plugins');
	
	/**
	 * @var string contains the full file path to Applications folder
	 */
	define('APPLICATION_PATH', SERVER_ROOT.'/Applications');
	
	/**
	 * @var string contains the full file path to Tests folder
	 */
	define('TEST_PATH', SERVER_ROOT.'/Tests');
?>